package com.stark.liquid;

public class SoapServiceProxy implements com.stark.liquid.SoapService {
  private String _endpoint = null;
  private com.stark.liquid.SoapService soapService = null;
  
  public SoapServiceProxy() {
    _initSoapServiceProxy();
  }
  
  public SoapServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initSoapServiceProxy();
  }
  
  private void _initSoapServiceProxy() {
    try {
      soapService = (new com.stark.liquid.SoapServiceServiceLocator()).getSoapService();
      if (soapService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)soapService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)soapService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (soapService != null)
      ((javax.xml.rpc.Stub)soapService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.stark.liquid.SoapService getSoapService() {
    if (soapService == null)
      _initSoapServiceProxy();
    return soapService;
  }
  
  public com.stark.liquid.SoapClass getuser(int id) throws java.rmi.RemoteException{
    if (soapService == null)
      _initSoapServiceProxy();
    return soapService.getuser(id);
  }
  
  public boolean registerUser(com.stark.rs.bean.User su) throws java.rmi.RemoteException{
    if (soapService == null)
      _initSoapServiceProxy();
    return soapService.registerUser(su);
  }
  
  
}