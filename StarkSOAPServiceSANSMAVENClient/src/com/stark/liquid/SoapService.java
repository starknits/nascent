/**
 * SoapService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.stark.liquid;

public interface SoapService extends java.rmi.Remote {
    public com.stark.liquid.SoapClass getuser(int id) throws java.rmi.RemoteException;
    public boolean registerUser(com.stark.rs.bean.User su) throws java.rmi.RemoteException;
}
