package com.stark.rs;

import java.sql.Connection;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stark.rs.bean.Accounts;
import com.stark.utils.RestUtils;

@RestController
@RequestMapping(value="/rest/")
public class AccountController {
	
	AccountService as = new AccountService();
	RestUtils rs = new RestUtils();
	Connection con = rs.getConnection();
	
	@RequestMapping(value="/service/accounts/{id}",method = RequestMethod.GET)
	public List<Accounts> getAllAccounts(@RequestHeader("Authorization") String authString,@PathVariable int id)
		{
		if(con==null)
			con = rs.getConnection();
		if(!RestUtils.isAccountValid(con,authString,id))
		{
			throw new RuntimeException("Invalid Username/ password"+authString);
		}
		return as.getAllAccounts(con, id);
		}
/*	@RequestMapping(value="/",method = RequestMethod.POST)
	public User createUser(@RequestBody User myUser)
	{
		if(con==null)
				con = rs.getConnection();
		User newUser=null;// = userService.registerUser(con, myUser);
		return newUser;
	}
	

	@RequestMapping(value="/service/banks/{id}" ,method = RequestMethod.GET)
	public Banks retrieveBank(@RequestHeader("Authorization") String authString,@PathVariable int id) {
		if(con==null)
			con = rs.getConnection();
		
		if(!RestUtils.isValid(con,authString))
		{
			throw new RuntimeException("Invalid Username/ password"+authString);
		}
		
		Banks banks = bs.getBank(con, id);
		if (banks==null)
	    throw new RuntimeException("user not found-" + id);
	return banks;
	}
*/	
}
