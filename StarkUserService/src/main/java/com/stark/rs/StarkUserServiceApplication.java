package com.stark.rs;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Properties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages="com.stark.rs")
public class StarkUserServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(StarkUserServiceApplication.class, args);
		customInitializer();
	}
	
	public static void customInitializer()
	{
		Connection connection = null;
		FileInputStream fis = null;	
		try{
		Class.forName("com.mysql.jdbc.Driver");
		Properties prop = new Properties();
		fis = new FileInputStream("src/main/resources/application.properties");
		prop.load(fis);
		String dburl = prop.getProperty("stark.dburl");
		String dbuser = prop.getProperty("stark.dbuser");
		String dbpassword = prop.getProperty("stark.dbpassword");
		connection = DriverManager.getConnection(dburl,dbuser,dbpassword);
		
		String userStr = "create table users(muid INT NOT NULL AUTO_INCREMENT, username varchar(100),password varchar(100),PRIMARY KEY(MUID),constraint uq unique(username))";
		String bankStr = "create table banks(muid INT NOT NULL AUTO_INCREMENT, name VARCHAR(100), url varchar(1000), username varchar(1000),password varchar(1000),primary key(muid))";
		String accountStr = "create table account(bankid int not null,accountno int not null auto_increment primary key,debit float,credit float,balance float)";

		String b1 ="INSERT INTO banks(muid,name,url,username,password)values(1,'ALLBK','ALBK.IO','ALUSER','ALPWD')";
		String b2="INSERT INTO banks(muid,name,url,username,password)values(2,'BOA','BOAK.IO','bALUSER','BALPWD')";
		String b3="INSERT INTO banks(muid,name,url,username,password)values(3,'CBK','cBK.IO','CALUSER','CALPWD')";
		
		
	String ai1="insert into account(bankid,accountno ,debit ,credit ,balance )values(1,1123,100,200,300)";
	String ai2="insert into account(bankid,accountno ,debit ,credit ,balance )values(2,2123,100,200,300) ";
	String ai3="insert into account(bankid,accountno ,debit ,credit ,balance )values(1,3123,100,200,300) ";
	String ai4="insert into account(bankid,accountno ,debit ,credit ,balance )values(1,4123,100,200,300) ";
	String ai5="insert into account(bankid,accountno ,debit ,credit ,balance )values(1,5123,100,200,300) ";
	String ai6="insert into account(bankid,accountno ,debit ,credit ,balance )values(1,6123,100,200,300) ";
	String ai7="insert into account(bankid,accountno ,debit ,credit ,balance )values(1,7123,100,200,300) ";
	String ai8="insert into account(bankid,accountno ,debit ,credit ,balance )values(3,8123,100,200,300) ";
	String ai9="insert into account(bankid,accountno ,debit ,credit ,balance )values(3,9123,100,200,300) ";
	String ai10="insert into account(bankid,accountno ,debit ,credit ,balance )values(3,99123,100,200,300)";

	String u1 = "insert into users(username,password) values ('AA','BB')";
	String u2 = "insert into users(username,password) values ('AAWQ','BBCC')";
	Statement stmt = connection.createStatement();
		try {
		
		String reset = prop.getProperty("reset");
		if("true".equals(reset))
		{
			String sql = "drop table account";
			try {
				stmt.executeUpdate(sql);
			}catch(Exception e)
			{
				
			}
			
			sql = "drop table banks";
			try {
				stmt.executeUpdate(sql);
			}catch(Exception e)
			{
				
			}
			
			sql = "drop table users";
			try {
				stmt.executeUpdate(sql);
			}catch(Exception e)
			{
				
			}
		}
		stmt.executeUpdate(userStr);
		stmt.executeUpdate(bankStr);
		stmt.executeUpdate(accountStr);
		
		
		stmt.executeUpdate(b1);
		stmt.executeUpdate(b2);
		stmt.executeUpdate(b3);
		
		stmt.executeUpdate(ai1);
		stmt.executeUpdate(ai2);
		stmt.executeUpdate(ai3);
		stmt.executeUpdate(ai4);
		stmt.executeUpdate(ai5);
		stmt.executeUpdate(ai6);
		stmt.executeUpdate(ai7);
		stmt.executeUpdate(ai8);
		stmt.executeUpdate(ai9);
		stmt.executeUpdate(ai10);
		
		stmt.executeUpdate(u1);
		stmt.executeUpdate(u2);
		
		}
		catch(Exception e)
		{
			
		}
		
		}
		catch(Exception e)
		{
			
		}
	}
}
