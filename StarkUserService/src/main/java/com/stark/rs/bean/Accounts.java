package com.stark.rs.bean;

public class Accounts {
	public Accounts(int bankid, int accountno, float debit, float credit, float balance) {
		super();
		this.bankid = bankid;
		this.accountno = accountno;
		this.debit = debit;
		this.credit = credit;
		this.balance = balance;
	}

	private int bankid;
	private int accountno;
	private float debit ;
	private float credit;
	private float balance;

	public Accounts() {}
	
	
	
	public int getBankid() {
		return bankid;
	}

	public void setBankid(int bankid) {
		this.bankid = bankid;
	}

	public int getAccountno() {
		return accountno;
	}

	public void setAccountno(int accountno) {
		this.accountno = accountno;
	}

	public float getDebit() {
		return debit;
	}

	public void setDebit(float debit) {
		this.debit = debit;
	}

	public float getCredit() {
		return credit;
	}

	public void setCredit(float credit) {
		this.credit = credit;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}
	
	
	
}
