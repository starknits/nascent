package com.stark.rs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.stark.rs.bean.Accounts;
import com.stark.rs.bean.Banks;

public class AccountService {
	
	public List<Accounts> getAllAccounts(Connection con, int bankid)
	{
		
		List<Accounts> accountList = new ArrayList<Accounts>();
		String query = "select bankid,accountno,debit,credit,balance from account where bankid =?";
		if(con!=null)
		{
			try {
				PreparedStatement ps = con.prepareStatement(query);
				ps.setInt(1, bankid);
				ResultSet result = ps.executeQuery();
				while(result.next())
				{
				Accounts  accounts = new Accounts(result.getInt(1),
										result.getInt(2),
									result.getFloat(3),
									result.getFloat(4),
									result.getFloat(5));
				accountList.add(accounts);
				}
				} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			finally {
				//try {if (!con.isClosed()) con.close();}
				//catch(Exception e) {}
			}
		}
		else
		{
			throw new RuntimeException("DB inacessible");
		}
		return accountList;
	}
	
	
//	
//	public Banks getBank(Connection con,int uid)
//	{
//		String query = "select muid,name,url,username,password from banks where muid="+uid;
//		Banks banks=null;
//		if(con!=null)
//		{
//			try {
//				PreparedStatement ps = con.prepareStatement(query);
//				ResultSet result = ps.executeQuery();
//				while(result.next())
//				{
//				banks = new Banks(result.getString(2),
//									result.getString(3),
//									result.getString(4),
//									result.getString(5));
//				banks.setId(result.getInt(1));
//				break;
//				}
//				} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//				throw new RuntimeException(e);
//			}
//			finally {
//				//try {if (!con.isClosed()) con.close();}
//				//catch(Exception e) {}
//			}
//		}
//		else
//		{
//			throw new RuntimeException("DB inacessible");
//		}
//		return banks;
//	}
}
