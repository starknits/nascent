package com.stark.rs;

import java.io.IOException;
import java.sql.Connection;
import java.util.Base64;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stark.rs.bean.Banks;
import com.stark.rs.bean.User;
import com.stark.utils.RestUtils;

@RestController
@RequestMapping(value="/rest/")
public class BankController {
	
	BankService bs = new BankService();
	RestUtils rs = new RestUtils();
	Connection con = rs.getConnection();
	
	@RequestMapping(value="/service/banks",method = RequestMethod.GET)
	public List<Banks> getAllBanks(@RequestHeader("Authorization") String authString)
		{
		if(con==null)
			con = rs.getConnection();
		if(!RestUtils.isValid(con,authString))
		{
			throw new RuntimeException("Invalid Username/ password"+authString);
		}
		return bs.getAllBanks(con);
		}
	@RequestMapping(value="/",method = RequestMethod.POST)
	public User createUser(@RequestBody User myUser)
	{
		if(con==null)
				con = rs.getConnection();
		User newUser=null;// = userService.registerUser(con, myUser);
		return newUser;
	}
	

	@RequestMapping(value="/service/banks/{id}" ,method = RequestMethod.GET)
	public Banks retrieveBank(@RequestHeader("Authorization") String authString,@PathVariable int id) {
		if(con==null)
			con = rs.getConnection();
		
		if(!RestUtils.isValid(con,authString))
		{
			throw new RuntimeException("Invalid Username/ password"+authString);
		}
		
		Banks banks = bs.getBank(con, id);
		if (banks==null)
	    throw new RuntimeException("user not found-" + id);
	return banks;
	}
	
}
