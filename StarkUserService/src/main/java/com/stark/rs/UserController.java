package com.stark.rs;

import java.sql.Connection;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stark.rs.bean.User;
import com.stark.utils.RestUtils;

@RestController
@RequestMapping(value="/rest/service")
public class UserController {
	
	UserService userService = new UserService();
	RestUtils rs = new RestUtils();
	Connection con = rs.getConnection();
	
	@RequestMapping(value="/users",method = RequestMethod.GET)
	public List<User> getAllUsers()
		{
		if(con==null)
			con = rs.getConnection();
		return userService.getAllUsers(con);
		}
	@RequestMapping(value="/users",method = RequestMethod.POST)
	public User createUser(@RequestBody User myUser)
	{
		if(con==null)
				con = rs.getConnection();
		User newUser = userService.registerUser(con, myUser);
		return newUser;
	}
	

	@RequestMapping(value="/users/{id}" ,method = RequestMethod.GET)
	public User retrieveUser(@PathVariable int id) {
		if(con==null)
			con = rs.getConnection();
	  User user = userService.retreiveUser(con, id);
	  if (user==null)
	    throw new RuntimeException("user not found-" + id);
	return user;
	}
	

	
}
