package com.stark.rs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.stark.rs.bean.Banks;
import com.stark.rs.bean.User;

public class BankService {
	
	public List<Banks> getAllBanks(Connection con)
	{
		
		List<Banks> bankList = new ArrayList<Banks>();
		String query = "select muid,name,url,username,password from banks";
		if(con!=null)
		{
			try {
				PreparedStatement ps = con.prepareStatement(query);
				ResultSet result = ps.executeQuery();
				while(result.next())
				{
				Banks banks = new Banks(result.getString(2),
									result.getString(3),
									result.getString(4),
									result.getString(5));
				banks.setId(result.getInt(1));
				bankList.add(banks);
				}
				} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			finally {
				//try {if (!con.isClosed()) con.close();}
				//catch(Exception e) {}
			}
		}
		else
		{
			throw new RuntimeException("DB inacessible");
		}
		return bankList;
	}
	
	
	
	public Banks getBank(Connection con,int uid)
	{
		String query = "select muid,name,url,username,password from banks where muid="+uid;
		Banks banks=null;
		if(con!=null)
		{
			try {
				PreparedStatement ps = con.prepareStatement(query);
				ResultSet result = ps.executeQuery();
				while(result.next())
				{
				banks = new Banks(result.getString(2),
									result.getString(3),
									result.getString(4),
									result.getString(5));
				banks.setId(result.getInt(1));
				break;
				}
				} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			finally {
				//try {if (!con.isClosed()) con.close();}
				//catch(Exception e) {}
			}
		}
		else
		{
			throw new RuntimeException("DB inacessible");
		}
		return banks;
	}
}
